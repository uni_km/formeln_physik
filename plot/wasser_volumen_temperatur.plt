set term png size 480,320
set output "wasser_volumen_temperatur.png"
set style line 1 lc rgb '#0060ad' linetype 1 linewidth 2 pt 7 pi -1 ps 1
#set pointintervalbox 3
#set title "Wasser Volumen/Temperatur"
set xlabel "T (K)"
set ylabel "V (m^3)"

plot "-" using 1:2 title "" with lines ls 1
-100  90
-90   90.5
-80   91
-70   91.5
-60   92
-50   93
-40   94
-30   95.5
-20   97
-10   99
0     100
1     99.5
2     99
3     0.5
4     0
10    0.25
20    0.5
30    1
40    2
50    4
60    8
70    16
80    32
90    64
100   128
e
