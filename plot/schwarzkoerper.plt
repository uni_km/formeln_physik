set term png size 480,320
set output "schwarzkoerper.png"
set style line 1 lc rgb '#ff0000' linetype 1 linewidth 2 pt 7 pi -1 ps 1
set style line 2 lc rgb '#0000ff' linetype 1 linewidth 2 pt 7 pi -1 ps 1
#set pointintervalbox 3
#set title "Schwarzkoerper"
set xlabel "Wellenlaenge"
set ylabel "E_SK"
set xrange [-2:5]
set yrange [0:1.2]
#set ytics("-WA" -10, 0 0)
Gauss(x,mu,sigma) = 1./(sigma*sqrt(2*pi)) * exp( -(x-mu)**2 / (2*sigma**2) )

sonne(x) = 3*Gauss(x, 1, 1)
erde(x)  = Gauss(x, 1.5, 1.)

plot  sonne(x) title "Sonne" with lines ls 1, \
      erde(x) title "Erde" with lines ls 2

