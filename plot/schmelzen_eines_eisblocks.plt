set term png size 480,320
set output "schmelzen_eines_eisblocks.png"
set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1.5
set pointintervalbox 3
set title "Schmelzen eines Eisblocks"
set xlabel "Q (kJ)"
set ylabel "T (deg C)"

plot "-" using 1 title "" with linespoints ls 1
-100
0
0
100
100
200
e
