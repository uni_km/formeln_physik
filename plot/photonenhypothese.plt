set term png size 480,320
set output "photonenhypothese.png"
set style line 1 lc rgb '#0060ad' linetype 1 linewidth 2 pt 7 pi -1 ps 1
#set pointintervalbox 3
#set title "Wasser Volumen/Temperatur"
set xlabel "f"
set ylabel "eU"
set ytics("-WA" -10, 0 0)

plot "-" using 1:2 title "" with lines ls 1
0   -10
10  0
100 90
